#ExportCloverTheme Script
##Adobe Illustrator script extension for export Clover vector themes

###How to use
* Design theme with Adobe Illustrator;
* Download `ExportCloverTheme.jsx` script and put it to Desktop;
* In Illustrator's **File** menu choose **Scrips - Other Script…** and select `ExportCloverTheme.jsx`;

Theme file `theme.svg` will be created near your source `.ai` file.

**Important note**
> Your source `.ai` file will be **saved** silently.

###Repository Content
* `ExportCloverTheme.jsx` - script source code;
* `Theme/CloverTheme.ai.zip` - example of source theme file;
* `Theme/RobotoCondensed-Regular.ttf` - font for theme source;
* `Theme/theme.svg` - theme file exported by script.
 
###Requirements
* Adobe Illustrator CC 2017 or never;
* Propertly constructed source `.ai` file.

###Links
* [**Clover**](https://sourceforge.net/projects/cloverefiboot/) - operating systems boot manager;
* [**Vector Themes**](https://www.insanelymac.com/forum/topic/334659-vector-themes/) discussion at insanelymac.com
* [**Clover Laboratory**](https://applelife.ru/threads/clover.42089/) forum at AppleLife.ru (in russian);
* [**How to create vector theme for Clover with Adobe Illustrator**](https://applelife.ru/threads/kak-sdelat-vektornuju-temu-v-adobe-illustrator.2943648) - short description theme structure and requirements for SVG-file (in russian);
* [**CloverSvgThemeBuilder**](https://bitbucket.org/blackosx/cloversvgthemebuilder) - other tool for build Clover themes.