// v0.8
// 
// Info: Illustrator CC 2015 Scripting Guide
// https://www.adobe.com/content/dam/acom/en/devnet/illustrator/pdf/AI_ScriptGd_2017.pdf

var content = "";

try {
	if (app.documents.length > 0 ) {
		// Illustrator document
		var sourceDoc = app.activeDocument;
		// Parent folder for document
		var destFolder = sourceDoc.path;
		
		if (destFolder != null) {
	  	
			// Save source .ai file
	    sourceDoc.save();
	    // Store source path for open after SaveAs… because Illustrator's export to SVG 
	    // is a 'Save As' actually.
	    var sourcePath = sourceDoc.fullName;
	    
	    // Get theme prefernces from text object named CloverThemePreferences.
	    var themePrefs = readThemePrefences();
			if (themePrefs == "") { alert("No CloverThemePreferences defined.") }
			
			// Get bbox for font (first font in theme only supported)
			var bboxValue = readThemeBbox();
			
	    // Get location and file object for target SVG
	    var targetFile = this.getTargetFile('theme', '.svg', destFolder);
	    
	    // Get theme options from text object
			var options = this.getOptions();
			
			// Export
	    sourceDoc.exportFile(targetFile, ExportType.SVG, options);
			// Now sourceDoc referenced to SVG file
			// We need close it and open actual source from stored path 
	    sourceDoc.close();
	    app.open(sourcePath);
	    
	    // Edit exported SVG
	    makeCorrections(targetFile, themePrefs, bboxValue);
	    
	    //	Optional open result SVG in default app (Safari)…
	    // targetFile.execute();
	    //	or display message.
			// alert( 'Export finished.' );
		}
	}
	else{
		throw new Error('There are no active document.');
	}
}
catch(e) {
	alert( e.message, "Export Clover Theme", true);
}

// SVG export options
//
function getOptions()
{
	var options = new ExportOptionsSVG();
	
// Requred
	
	options.compressed = false;
	options.coordinatePrecision = 3;
	options.documentEncoding = SVGDocumentEncoding.UTF8;
	options.DTD = SVGDTDVersion.SVG1_1;
	options.embedRasterImages = true;
	options.includeUnusedStyles = false;
	options.optimizeForSVGViewer = false;
	options.sVGTextOnPath = false;	
	// cssProperties: ENTITIES, PRESENTATIONATTRIBUTES, STYLEATTRIBUTES, STYLEELEMENTS
	options.cssProperties = SVGCSSPropertyLocation.STYLEELEMENTS;	
	// fontSubsetting: None, ALLGLYPHS, COMMONENGLISH, COMMONROMAN, GLYPHSUSED
	options.fontSubsetting = SVGFontSubsetting.ALLGLYPHS; 
	// fontType: SVGFONT, CEFFONT, OUTLINEFONT
	options.fontType = SVGFontType.SVGFONT;
	
// Other options
// 
//	options.includeFileInfo = false;
//	options.includeVariablesAndDatasets = true; // Default is false
//	options.preserveEditability = true; // Default is false
//	options.slices = true; // Default is false

//  For saveMultipleArtboards==true - IMPORTANT: any outside of arboard objects will be not exprorted.
//	options.saveMultipleArtboards = true; // Default is false
//	options.artboardRange = "1,3"; // comma separated numbers

options.sVGAutoKerning = true; // Default is false
//	options.includeVariablesAndDatasets = false;
	
	return options;
}

// Corrects exported SVG for Clover theme requirements
//
function makeCorrections(svgFile, themePrefs, bboxValue) {
	// source code of SVG
	content = readTextFile(svgFile);
	
	// Add Clover namespace
	content = content.replace("xmlns:xlink=\"http://www.w3.org/1999/xlink\"", "xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:clover=\"https://sourceforge.net/projects/cloverefiboot\"");
	
	// Add Clover theme preferences
	if (themePrefs == "") {
		themePrefs = "<clover:theme\n";
	} else {
		themePrefs = "<clover:theme\n" + themePrefs;
	}
	// Insert generation datetimestamp
	var genStamp = new Date();
	var genStampStr = 'Created="' + genStamp.toLocaleString() + '"'; //or toUTCString();
	themePrefs = themePrefs + '\n' + genStampStr + '/>';	
	content = content.replace("\n<style type", "\n" + themePrefs + "\n<style type");
	// Remove prefences text object from code
	removeTextObject("CloverThemePreferences"); 
	
	// Add bbox attribute
	var tagStartPos = content.indexOf("<font-face ");
	var bboxItemMissing = false;
	if ( bboxValue == "" ) { bboxItemMissing = true } // No CloverThemeFontBbox text object in source
	if ( tagStartPos > -1 ) {
		var tagClosePos = content.indexOf("/>", tagStartPos);
		if ( tagClosePos > -1 ) {
			if ( bboxItemMissing ) { 
				// Make default bbox values based on units-per-em.
				var attrStartPos = content.indexOf("units-per-em", tagStartPos);
				if ( attrStartPos > -1 ) {
					var closeQuotePos = content.indexOf('"', attrStartPos + 14);
					if ( closeQuotePos > -1 ) {
						var upmValue = content.slice(attrStartPos + 14, closeQuotePos);
						var bboxX = upmValue * 0.11;
						var bboxY = upmValue * 0.17;
						var bboxW = upmValue - bboxX;
						var bboxH = upmValue - bboxY;
						bboxValue = 'bbox="-' + bboxX + " -" + bboxY + " " + bboxW + " " + bboxH + '"'
					}
				}
			}
			// Write value
			if ( bboxValue != "" ) {
				content = content.slice(0, tagClosePos) + " " + bboxValue + " " + content.slice(tagClosePos);
			}
		}
		if ( !bboxItemMissing ) {
			// Remove bbox text object
			removeTextObject("CloverThemeFontBbox");
		}
	}
	
	// Removing unused glyphs
	removeUnusedGlyphs();
	
	// Removing enable-background attribute
	var regex51 = /    /gi;
	content = content.replace(regex51, '');
	regex51 = /enable-background:new;/gi;
	content = content.replace(regex51, '');
	
	// Add visibility="hidden" attribute to all BoundingRects
	var regex = /id="BoundingRect/gi;
	content = content.replace(regex, 'visibility="hidden" id="BoundingRect');
		
	// replace all "_x5F_" sequences to "_"
	// Illustrator can replace underscpe symbols in names of objects.
	// We need revert these replacements.
	var regex3 = /_x5F_/gi;
	content = content.replace(regex3, '_');
	
	// Remove font-family quotes
	// Clover will be ignore font with single quoted name in <style> section,
	// we need remove quotes.
	var fontStyleStartPos = content.indexOf("{font-family:'");
	if (fontStyleStartPos > -1) {
		var keyLen = 14;
		var quote2Pos = content.indexOf("'", fontStyleStartPos + keyLen);
		if (quote2Pos > -1) {
			content = content.slice(0, fontStyleStartPos + keyLen - 1) + content.slice(fontStyleStartPos + keyLen, quote2Pos) + content.slice(quote2Pos + 1);
		}
	}
	
	// Remove <xxx id="_yyy"
	// If we don't want export some object from our design we can set first char 
	// of this object name to underscope.
	var hiddenIdPos = content.indexOf('id="_');
	while (hiddenIdPos > -1) {
		var tagStartPos = hiddenIdPos - 1;
		do {
			tagStartPos--;
		} while (content.charAt(tagStartPos) != "<")
		
		var isRemoved = removeTagWithContent(tagStartPos);
		if ( isRemoved ) {
			hiddenIdPos = tagStartPos;
		} else {
			alert(content.slice(hiddenIdPos, tagStartPos + 30));
			hiddenIdPos++;
		}
		hiddenIdPos = content.indexOf('id="_', hiddenIdPos);		
	}
	
	// Add Clover attributes to groups named as "groupName&attrName:attrValue"
	// AttrName length must be from 1 to 64.
	//
	// Now usable for dithering only (see Backgrounds groups in .AI source)
	var ampPos = content.indexOf('_x26_');
	while (ampPos > -1) {
		// find tag open mark
		var p = ampPos - 1;
		while ( content.charAt(p) != "<" ) { p--; }
		var tagStPos = p;
		// find space after tag name
		p++;
		while ( content.charAt(p) != " " ) { p++; }
		var tagName = content.slice(tagStPos + 1, p);
		// find ":"
		p = ampPos + 5;
		if ( ( content.indexOf(':', ampPos ) < ampPos + 64) && ( content.indexOf(':', ampPos) > -1 ) ) {
			while ( content.charAt(p) != ":" ) { p++; }
			var attrName = content.slice(ampPos + 5, p);
			p++;
			var valueStart = p;
			while ( content.charAt(p) != '"' ) { p++; }
			var attrValue = content.slice(valueStart, p);
			//alert("Attribute for " + tagName + ": " + attrName + "=" + attrValue);
		
			// find tag close mark
			var tagCloseValue = "</" + tagName + ">";
			var shortClosed = " path rect circle ellipse line polyline polygon ";
			if ( shortClosed.indexOf(" " + tagName + " ") > -1 ) { tagCloseValue = "/>" }
			var tagClosePos = content.indexOf(tagCloseValue, p);
			//alert(content.slice(tagStPos, tagClosePos));
			
			var tagContent = "";
			switch (attrName) {
			case 'dither':
				// dither for radialGradient & linearGradient childrens of current group
				tagContent = content.slice(tagStPos, tagClosePos + tagCloseValue.length);				
				var regex = /<radialGradient/gi;
				tagContent = tagContent.replace(regex, '<radialGradient ' + 'clover:ditherCoarse="' + attrValue + '" ');
				//alert(tagContent);
				regex = /<linearGradient/gi;
				tagContent = tagContent.replace(regex, '<linearGradient ' + 'clover:ditherCoarse="' + attrValue + '" ');
				// 
				
				break;
			}
			// remove attribute suffix from id
			var idSuffixStart = tagContent.indexOf('_x26_');
			var idQuote2 = tagContent.indexOf('"', idSuffixStart);
			tagContent = tagContent.slice(0, idSuffixStart) + tagContent.slice(idQuote2);
			
			// replace content
			content = content.slice(0, tagStPos) + tagContent + content.slice(tagClosePos + tagCloseValue.length);
		
			ampPos = content.indexOf('_x26_', tagClosePos);
		
		} else {
			ampPos = -1;
		}
	}
	
	
	
	
	
	// Done!
	//
	// Save changed svg
	saveTextFile(svgFile, content);
}

// Shorten font
function removeUnusedGlyphs() {
	// Charcodes for unwanted glyps
	var glyphsToRemove = ['18F', '192', '1A0', '1A1', '1AF', '1B0', '237', '259', '2C6', '2C7', '2C9', '2D8', '2D9', '2DA', '2DB', '2DC', '2DD', '2F3', '323', '384', '385', '387', '454', '459', '45A', '45B', '45F', '460', '461', '463', '464', '465', '466', '467', '468', '469', '46A', '46B', '46C', '46D', '46E', '46F', '472', '473', '474', '475', '47A', '47B', '47C', '47D', '47E', '47F', '480', '481', '482', '483', '484', '485', '486', '488', '48A', '48B', '48D', '48E', '48F', '490', '491', '494', '495', '496', '497', '49A', '49B', '49C', '49D', '4A0', '4A1', '4A2', '4A3', '4A4', '4A5', '4A6', '4A7', '4A8', '4A9', '4B2', '4B3', '4B4', '4B5', '4B6', '4B7', '4B8', '4B9', '4BA', '4BC', '4BD', '4C3', '4C4', '4C5', '4C6', '4C7', '4C8', '4C9', '4CA', '4CD', '4CE', '4D8', '4E0', '4E1', '4FA', '4FB', '4FC', '4FD', '500', '502', '503', '504', '505', '506', '507', '508', '509', '50A', '50B', '50C', '50D', '50E', '50F', '510', '512', '513', '210', '211', '211', '212', '212', '215', '215', '215', '215', '218', '219', '160', '161', '21A', '21B', '450', '45D', '470', '471', '476', '477', '479', '478', '498', '499', '4AA', '4AB', '4AE', '4AF', '4C0', '4C1', '4C2', '4CF', '4D0', '4D1', '4D2', '4D3', '4D4', '4D5', '4D6', '4D7', '4DA', '4D9', '4DB', '4DC', '4DD', '4DE', '4DF', '4E2', '4E3', '4E4', '4E5', '4E6', '4E7', '4E8', '4E9', '4EA', '4EB', '4EC', '4ED', '4EE', '4EF', '4F0', '4F1', '4F2', '4F3', '4F4', '4F5', '4F8', '4F9', '501', '1EA0', '1EA1', '1EA2', '1EA3', '1EA4', '1EA5', '1EA6', '1EA7', '1EA8', '1EA9', '1EAA', '1EAB', '1EAC', '1EAD', '1EAE', '1EAF', '1EB0', '1EB1', '1EB2', '1EB3', '1EB4', '1EB5', '1EB6', '1EB7', '1EB8', '1EB9', '1EBA', '1EBB', '1EBC', '1EBD', '1EBE', '1EBF', '1EC0', '1EC1', '1EC2', '1EC3', '1EC4', '1EC5', '1EC6', '1EC7', '1EC8', '1EC9', '1ECA', '1ECB', '1ECC', '1ECD', '1ECE', '1ECF', '1ED0', '1ED1', '1ED2', '1ED3', '1ED4', '1ED5', '1ED6', '1ED7', '1ED8', '1ED9', '1EDA', '1EDB', '1EDC', '1EDD', '1EDE', '1EDF', '1EE0', '1EE1', '1EE2', '1EE3', '1EE4', '1EE5', '1EE6', '1EE7', '1EE8', '1EE9', '1EEA', '1EEB', '1EEC', '1EED', '1EEE', '1EEF', '1EF0', '1EF1', '1EF4', '1EF5', '1EF6', '1EF7', '1EF8', '1EF9', '2105', '2113', '2116', '2122', '212E', '215B', '215C', '215D', '215E', '4AC', '4AD', '4CB', '4CC', '4F6', '4F7', '4BE', '4BF', '4BB', '48C', '462', '492', '493', '49E', '49F', '4B0', '4B1', '4FE', '4FF', '511', '1F4D']
	
	for ( var i = 0; i < glyphsToRemove.length; i++ ) {
		var glyphStart = content.indexOf('<glyph unicode="&#x' + glyphsToRemove[i] + ";");
		if ( glyphStart > -1 ) {
			var glyphEnd = content.indexOf('/>', glyphStart + 1);
			if ( glyphEnd > -1 ) {
				content = content.slice(0, glyphStart) + content.slice(glyphEnd + 3);
			}
		}
	}
}

//
// Utilities
//

// Reads file and returns string content
function readTextFile(fileObj) {
	fileObj.open("read");
	var content = fileObj.read();
	fileObj.close();
	return content;
}

// Save file
function saveTextFile(fileObj, content) {
	fileObj.open("write");
	fileObj.write(content);
	fileObj.close();
}

// Returns content of theme options from text object
function readThemePrefences() {
	var prefsData = "";
	var docItems = app.activeDocument.pageItems;
// 	try {
// 		var prefsItem = docItems.getByName("CloverThemePreferences");
// 		prefsData = prefsItem.contents;
// 	}
// 	catch(e) {
// 		alert( e.message, "readThemePrefences", true);
// 	}
	for ( i = 0; i < docItems.length; i++ ) {
		var prefsItem = docItems[i];
		if ( prefsItem.name == "CloverThemePreferences" && prefsItem.typename == "TextFrame" ) {
			prefsData = prefsItem.contents;
			break;
		}
	}
	return prefsData;
}

// Returns CloverThemeFontBbox content.
// Supports one font in theme only.
function readThemeBbox() {
	var bboxData = "";
	var docItems = app.activeDocument.pageItems;
	for ( i = 0; i < docItems.length; i++ ) {
		var currItem = docItems[i];
		if ( currItem.name == "CloverThemeFontBbox" && currItem.typename == "TextFrame" ) {
			bboxData = currItem.contents;
			break;
		}
	}
	return bboxData;
}

// Removes TEXT tag with its internal content
function removeTextObject(id) {
	var delStart = content.indexOf("<text id=\"" + id + "\"");
	if ( delStart > -1 ) {
		var rectStart = delStart - 4;
		while ( (content.slice(rectStart, rectStart + 6) != "<rect ") && (rectStart > -1) ) {
			rectStart--;
		}
		var delEnd = content.indexOf("</text>", delStart);
		if ( delEnd > -1 ) {
			if ( rectStart> -1 ) { delStart = rectStart }
			content = content.slice(0, delStart - 1) + content.slice(delEnd + 7);
		}
	}
}

// Returns new file reference
// 	docName - name of file
// 	ext - file extension
// 	trgFolder - parent folder for file
function getTargetFile(docName, ext, trgFolder) {
	var newName = "";
	if (docName.indexOf('.') < 0) {
		newName = docName + ext;
	} else {
		var dotPos = docName.lastIndexOf('.');
		newName = newName + docName.substring(0, dotPos) + ext;
	}
	var trgFile = new File(trgFolder + '/' + newName);
	// can write?
	if (trgFile.open("w")) {
		trgFile.close();
	}
	else {
		throw new Error('Access is denied for ' + trgFolder + "/" + docName + "." + ext);
	}
	return trgFile;
}


// Removes tag with inner content
function removeTagWithContent(startPos) {
	var tagRemoved = false;
	if (content.charAt(startPos) == "<" ) {
		var oCount = 1;
		var cCount = 0;
		var pos = startPos + 1;
		var notLast = false;
		
		while (((oCount > cCount) || notLast) && pos<content.length) {
			if ( content.charAt(pos) == "<" ) {
				if ( content.charAt(pos + 1) == "/" ) {
					cCount++;
					if (oCount == cCount) {notLast = true;}
				} else {
					oCount++;
				}
			}
			else if (content.charAt(pos) == ">" ) {
				if ((cCount == oCount) && notLast) {
					notLast = false;
					pos--;
				}
				else if (content.charAt(pos - 1) == "/" ) {
					cCount++;
				}
			}
			pos++;
		}
		if (oCount == cCount) {
			// removing
			content = content.slice(0, startPos) + content.slice(pos + 1);
			tagRemoved = true;
		} else {
			alert( "Can't remove tag: " + content.slice(startPos, 80) );
		}
	}
	return tagRemoved;
}
